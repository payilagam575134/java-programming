package typecasting;
public class Employee extends Student{
	public static void main(String[] args) 
	{
      Student st=new Employee();
      Employee emp=(Employee)st;    //down casting
      emp.project();
      emp.study();
//      Student st=(Student)emp;    //Up casting 
      st.study();
      System.out.println(st);
	}
public void project() 
{
	System.out.println("Projects");
}
}