package interfacee;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
public class Filehandling {
	public static void main(String[] args) throws IOException {
		File ff=new File("/home/jawahar/Desktop/Web Design/HTML/jawa.txt");
		try {
			System.out.println(ff.createNewFile());
		} catch (IOException e) {
			e.printStackTrace();
		}
		   if(ff.canWrite())           //file writer
		   {
			   FileWriter fire= new FileWriter(ff,true);
			   BufferedWriter obj=new BufferedWriter(fire);  //buffered writer
//			   fire.write("All");
//			   fire.write("is");
//			   fire.write("well"+ "  ");
			   obj.write("Jawa the boss");
//			   fire.flush();
//			   fire.close();
			   obj.flush();
			   obj.close();
		   }
		   if(ff.canRead())           //file reading
		   {
			   FileReader reader= new FileReader(ff);
			   BufferedReader bf= new BufferedReader(reader);
			   String ch=bf.readLine();
			   while(ch!=null)
			   {
				   System.out.println(ch);
				   ch=bf.readLine();
			   }
//		File[] ar=ff.listFiles();         //listing the no of files
//		for(int i=0;i<ar.length;i++)
//		{
//			if(ar[i].isFile()) {
//				String str=ar[i].getName();    //file with same type
//				if(str.endsWith(".html")) {
//			System.out.println(ar[i].getName());}
//			}
		   }}}