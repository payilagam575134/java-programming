package learncomparable;

public class Mobiles implements Comparable {
     int price,ram,px;
	public Mobiles(int price, int ram, int px) 
	{
		this.price=price;
		this.ram=ram;
		this.px=px;
	}

	public static void main(String[] args) {
	 Mobiles samsung=new Mobiles(19000,6,12);   
	 Mobiles oppo=new Mobiles(19000,8,32);
	 int result=samsung.compareTo(oppo);
	 System.out.println(result);
	}
	@Override
	public int compareTo(Object obj) {
		Mobiles oppo= (Mobiles)obj;
		if(this.price>oppo.price)
		{
			return +1;
		}
		else if(this.price<oppo.price)
		{
			return -1;
		}
		return 0;
	}
}