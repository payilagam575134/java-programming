public class Comfort
{
	String name;
	int floors;
	int persons;
	public Comfort(String name,int floors,int persons)
	{
		this.name=name;
		this.floors=floors;
		this.persons=persons;
	}
	
	public static void main(String[] args)
	{
		Comfort obj1=new Comfort("abc",1,5);
		Comfort obj2=new Comfort("bcd",2,8);
		Comfort obj3=new Comfort("cde",3,7);
		Comfort obj4=new Comfort("def",4,10);
		Comfort obj5=new Comfort("efg",5,20);
		obj1.display();
		obj2.display();
		obj3.display();
		obj4.display();
		obj5.display();
	}
	public void display()
	{
		System.out.println(this.name);
		System.out.println(this.floors);
		System.out.println(this.persons);
	}
	

}
